# HAL Video Audio Setup für Streaming #




## Video ##

1. [Capturkarte HDMI "Elgato HD60s"](https://www.elgato.com/de/gaming/game-capture-hd60-s "Capturkarte HDMI "Elgato HD60s"") Bildschirmcapture des Vortrags PC
2. [Logitech HD-Pro-Webcam-c920](https://www.logitech.com/de-de/product/hd-pro-webcam-c920) Aufnahme des Redners


## Audio ##



1. [AUNA MIC-900B-USB-Kondensator-Mikrofon-schwarz-Niere](https://www.auna.de/Mikrofone/Studio-Mikrofone/MIC-900B-USB-Kondensator-Mikrofon-schwarz-Niere-Studio.html) Tonaufnahme Redner/Publikum
2. [Funkmikrofon](https://www.thomann.de/de/the_tbone_tws_one_a_lapel.htm?ref=prod_rel_183401_4) Momentan nicht in Benutzung da sehr leise und verauscht. Noch keine Zeit für weitere Test, vielleicht lag es auch am Setup. Wer mag kann das gerne nochmal prüfen.


## Stream PC ##



- Windows 10 Home
- Intel® Core™ i7-6700HQ Prozessor Quad-Core 2,60 GHz
- NVIDIA® GeForce® GTX 950M mit 4 GB Dediziert Speicher
- 16 GB, DDR4 SDRAM
- 1 TB HDD, 512 GB SSD 


## Hardware Setup ##


![](/Bilder/Hardware_Setup.png)


## Software ##

- ### [obsproject wiki](https://obsproject.com/wiki/)

- Ablage Videoaufnahmen D:/VIDEO/
- Vorlage Ordner D:/OBS/

## Kurzbedienung ##

- Vortragenen Fragen nach seinen persönlichen Wünschen (Stream nur Folien, mit Bild , Mit Ton, Name im Stream etc.)
- Vortragen darauf Hinweisen alle Notifications zu deaktivieren, da sein kompletter Bildschirm aufgezeichnet wird
- Verkabeln nach Hardware Setup
- Streaming Rechner Starten
- Über VNC sich mit dem Streamingrechner verbinden (DNS: stream01) PW: wie immer
- Anmelden an WIN10 PW: wie immer
- OBS Software startet automatisch

![](/Bilder/OBS_1.PNG)

- auf der Szene "Vortrag_klassisch" die beiden Textfelder ändern (rechte Maustaste-->Eigenschaften)
- auf dem rechten Bildschirm ist das zu sehen was als Stream auf die HD bzw an Youtube LIVE geht
- Mit "Überblenden" wird der linke Bildschirm in den Stream übergeblendet
- Mikros wenn nötig aktiviren
- "Stream Starten" es wird auf Youtube Live gestreamt. Die Software ist im moment so konfiguriert das auch automatisch auf HDD gespeichert wird 


## HOTKEY ##

- STRG+1 Startbilschirm
- STRG+2 VortragsBildschirm
- STRG+3 PAUSE
- STRG+ü Überblenden