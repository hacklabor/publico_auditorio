﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TbSzeneCurrent = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtStop = New System.Windows.Forms.Button()
        Me.BtStart = New System.Windows.Forms.Button()
        Me.CbSzene = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CbUeberblenden = New System.Windows.Forms.ComboBox()
        Me.LbNächsteSzene = New System.Windows.Forms.Label()
        Me.RS1 = New System.IO.Ports.SerialPort(Me.components)
        Me.TbRxRS232 = New System.Windows.Forms.RichTextBox()
        Me.ScreenPrevTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.OFD1 = New System.Windows.Forms.OpenFileDialog()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.GbCom = New System.Windows.Forms.GroupBox()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'TbSzeneCurrent
        '
        Me.TbSzeneCurrent.Location = New System.Drawing.Point(294, 14)
        Me.TbSzeneCurrent.Name = "TbSzeneCurrent"
        Me.TbSzeneCurrent.Size = New System.Drawing.Size(264, 20)
        Me.TbSzeneCurrent.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(564, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Aktuelle Szene"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(281, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Nächste Szene"
        '
        'BtStop
        '
        Me.BtStop.Location = New System.Drawing.Point(815, 36)
        Me.BtStop.Name = "BtStop"
        Me.BtStop.Size = New System.Drawing.Size(75, 23)
        Me.BtStop.TabIndex = 1
        Me.BtStop.Text = "Stop"
        Me.BtStop.UseVisualStyleBackColor = True
        '
        'BtStart
        '
        Me.BtStart.Location = New System.Drawing.Point(815, 12)
        Me.BtStart.Name = "BtStart"
        Me.BtStart.Size = New System.Drawing.Size(75, 23)
        Me.BtStart.TabIndex = 0
        Me.BtStart.Text = "Start"
        Me.BtStart.UseVisualStyleBackColor = True
        '
        'CbSzene
        '
        Me.CbSzene.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbSzene.FormattingEnabled = True
        Me.CbSzene.Location = New System.Drawing.Point(12, 43)
        Me.CbSzene.Name = "CbSzene"
        Me.CbSzene.Size = New System.Drawing.Size(265, 21)
        Me.CbSzene.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 129)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(151, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Überblenden"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CbUeberblenden
        '
        Me.CbUeberblenden.FormattingEnabled = True
        Me.CbUeberblenden.Location = New System.Drawing.Point(12, 90)
        Me.CbUeberblenden.Name = "CbUeberblenden"
        Me.CbUeberblenden.Size = New System.Drawing.Size(265, 21)
        Me.CbUeberblenden.TabIndex = 10
        '
        'LbNächsteSzene
        '
        Me.LbNächsteSzene.AutoSize = True
        Me.LbNächsteSzene.Location = New System.Drawing.Point(132, 17)
        Me.LbNächsteSzene.Name = "LbNächsteSzene"
        Me.LbNächsteSzene.Size = New System.Drawing.Size(80, 13)
        Me.LbNächsteSzene.TabIndex = 11
        Me.LbNächsteSzene.Text = "Nächste Szene"
        '
        'RS1
        '
        Me.RS1.BaudRate = 57600
        Me.RS1.PortName = "COM7"
        '
        'TbRxRS232
        '
        Me.TbRxRS232.Location = New System.Drawing.Point(1086, 12)
        Me.TbRxRS232.Name = "TbRxRS232"
        Me.TbRxRS232.Size = New System.Drawing.Size(239, 243)
        Me.TbRxRS232.TabIndex = 13
        Me.TbRxRS232.Text = ""
        '
        'ScreenPrevTimer
        '
        Me.ScreenPrevTimer.Interval = 2000
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(815, 65)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "Start"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'OFD1
        '
        Me.OFD1.FileName = "OpenFileDialog1"
        '
        'dgv1
        '
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(12, 299)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.Size = New System.Drawing.Size(1313, 150)
        Me.dgv1.TabIndex = 15
        '
        'GbCom
        '
        Me.GbCom.Location = New System.Drawing.Point(528, 90)
        Me.GbCom.Name = "GbCom"
        Me.GbCom.Size = New System.Drawing.Size(200, 100)
        Me.GbCom.TabIndex = 16
        Me.GbCom.TabStop = False
        Me.GbCom.Text = "COM"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1337, 549)
        Me.Controls.Add(Me.GbCom)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TbRxRS232)
        Me.Controls.Add(Me.LbNächsteSzene)
        Me.Controls.Add(Me.CbUeberblenden)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CbSzene)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TbSzeneCurrent)
        Me.Controls.Add(Me.BtStop)
        Me.Controls.Add(Me.BtStart)
        Me.Name = "Form1"
        Me.Text = "OBS Remote"
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As Timer
    Friend WithEvents TbSzeneCurrent As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BtStop As Button
    Friend WithEvents BtStart As Button
    Friend WithEvents CbSzene As ComboBox
    Friend WithEvents Button1 As Button
    Friend WithEvents CbUeberblenden As ComboBox
    Friend WithEvents LbNächsteSzene As Label
    Friend WithEvents RS1 As IO.Ports.SerialPort
    Friend WithEvents TbRxRS232 As RichTextBox
    Friend WithEvents ScreenPrevTimer As Timer
    Friend WithEvents Button2 As Button
    Friend WithEvents OFD1 As OpenFileDialog
    Friend WithEvents dgv1 As DataGridView
    Friend WithEvents GbCom As GroupBox
End Class
