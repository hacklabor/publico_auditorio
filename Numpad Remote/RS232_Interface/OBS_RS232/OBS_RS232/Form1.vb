﻿Imports OBSWebsocketDotNet
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Web.Script.Serialization

Public Class Form1

    Dim obs As New OBSWebsocket
    Dim ObsVersion As OBSVersion
    Dim ObsSzene As OBSScene
    Dim ObsSzenenListe As List(Of OBSScene)
    Dim ObsBlendeListe As List(Of String)
    Dim Numlook As Boolean
    Dim KeyLastState(255) As Boolean
    Dim KeyCurrentState(255) As Boolean
    Dim PrevSzeneIndex As Int16 = 0
    Dim AktuallSzeneIndex As Int16 = 0
    Dim CbSzeneHidden As New ComboBox
    Dim spezForces As List(Of String)
    Dim DsSceneItem As DataSet
    Dim TabSceneItem As DataTable
    Dim letzterStatusIsRecording As Boolean = True
    Dim letzterStatusIsStreaming As Boolean = True


    Public Enum KEYS
        NUM0 = 96
        NUM1 = 97
        NUM2 = 98
        NUM3 = 99
        NUM4 = 100
        NUM5 = 101
        NUM6 = 120
        NUM7 = 103
        NUM8 = 104
        NUM9 = 105
        NUM_Punkt = 110
        N0 = 45
        N1 = 35
        N2 = 40
        N3 = 34
        N4 = 37
        N5 = 12
        N6 = 39
        N7 = 36
        N8 = 38
        N9 = 33
        N_Punkt = 46
        Div = 111
        Asterix = 106
        Plus = 107
        Minus = 109
        Enter = 13
        TabReturn = 8
        NUM = 144

    End Enum

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BtStart.Click
        obs.Disconnect()





    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        If My.Computer.Keyboard.NumLock Then
            Numlook = True
        Else
            Numlook = False
        End If
        ' Aktuellen KeyStatus Hollen
        For KeyNr = 8 To 144
            KeyAuswerten(KeyNr)
        Next
        KeyAction()
        obs.ListTransitions()
        ObsVersion = obs.GetVersion()
        ObsSzene = obs.GetCurrentScene
        TbSzeneCurrent.Text = ObsSzene.Name.ToString
        ObsSzene = obs.GetPreviewScene
        LbNächsteSzene.Text = ObsSzene.Name.ToString
        TxStatus()



    End Sub

    Private Sub KeyAction()
        If My.Computer.Keyboard.NumLock Then
            If KeyCurrentState(KEYS.Plus) = True Then prevIndex(1)
            If KeyCurrentState(KEYS.Minus) = True Then prevIndex(-1)
            If KeyCurrentState(KEYS.NUM0) = True Then Überblenden()
            If KeyCurrentState(KEYS.Div) = True Then obs.ToggleStreaming()
            If KeyCurrentState(KEYS.Asterix) = True Then obs.ToggleRecording()

            For i = 0 To CbSzene.Items.Count - 2
                If KeyCurrentState(KEYS.NUM1 + i) = True Then
                    CbSzene.Text = CbSzene.Items(i)
                    PrevSzeneIndex = i
                End If


            Next
        Else

        End If
    End Sub

    Private Sub prevIndex(Richtung As Integer)


        PrevSzeneIndex += Richtung
        If PrevSzeneIndex > CbSzene.Items.Count - 2 Then
            PrevSzeneIndex = 0
        End If
        If PrevSzeneIndex < 0 Then
            PrevSzeneIndex = CbSzene.Items.Count - 2
        End If
        CbSzene.Text = CbSzene.Items(PrevSzeneIndex)


    End Sub

    Private Function KeyAuswerten(KeyNr As Byte)
        Dim status As Boolean = getkey(KeyNr)

        If KeyLastState(KeyNr) = status Then
            KeyCurrentState(KeyNr) = False

            Return False
        End If
        If status = True Then
            KeyLastState(KeyNr) = True
            KeyCurrentState(KeyNr) = True
            Return True
        Else
            KeyLastState(KeyNr) = False
            KeyCurrentState(KeyNr) = False
            Return False
        End If
    End Function


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles BtStop.Click
        Timer1.Stop()
        obs.Disconnect()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Threading.Thread.Sleep(3000)
Start1: For Each Process1 In Process.GetProcesses
            Dim a As String
            a = Process1.ToString
            If a.ToUpper.IndexOf("OBS64") > -1 Then GoTo ende

        Next
        Process.Start("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\OBS Studio\OBS Studio (64bit)")
        GoTo Start1

ende:   Threading.Thread.Sleep(1000)
        Try

            obs.Connect("ws://127.0.0.1:4444", "HAL")
            ObsSzenenListe = obs.ListScenes
            ObsBlendeListe = obs.ListTransitions
            CbSzene.Items.Clear()
            CbSzeneHidden.Items.Clear()
            For i = 0 To ObsSzenenListe.Count - 1
                CbSzene.Items.Add(ObsSzenenListe(i).Name)
                CbSzeneHidden.Items.Add(ObsSzenenListe(i).Name)
            Next
            ObsSzene = obs.GetPreviewScene
            CbSzene.Text = CbSzene.Items(PrevSzeneIndex)
            CbUeberblenden.Items.Clear()

            For i = 0 To ObsBlendeListe.Count - 1
                CbUeberblenden.Items.Add(ObsBlendeListe(i))
            Next
            If obs.StudioModeEnabled = False Then
                obs.ToggleStudioMode()
            End If

            Dim port() As String = IO.Ports.SerialPort.GetPortNames()
            RS1.ReadTimeout = 200
            RS1.WriteTimeout = 200
            For i = 0 To port.Length - 1
                RS1.PortName = port(i)
                RS1.Open()
                For o = 0 To 4


                    If TxRS232("#Test") = "#Test" Then GoTo weiter
                Next
            Next
            MsgBox("Remote Panel an COM nicht gefunden")
weiter:
            If RS1.IsOpen = True Then
                Timer1.Start()
            End If


            BtStop.Enabled = True
        Catch ex As Exception
            MsgBox(ex.Message)
            Timer1.Stop()
            obs.Disconnect()
            BtStop.Enabled = False
            BtStart.Enabled = True
        End Try

    End Sub
    ' getkey, API call to USER32.DLL
    <DllImport("USER32.DLL", EntryPoint:="GetAsyncKeyState", SetLastError:=True,
    CharSet:=CharSet.Unicode, ExactSpelling:=True,
    CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function getkey(ByVal Vkey As Integer) As Boolean
    End Function

    Private Sub SzeneUP()

    End Sub





    Private Sub CbSzene_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbSzene.SelectedIndexChanged
        obs.SetPreviewScene(ObsSzenenListe(CbSzene.SelectedIndex))
        LbNächsteSzene.Text = ObsSzene.Name.ToString
        TxPreviewScreen()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Überblenden()
    End Sub

    Private Sub Überblenden()
        ScreenPrevTimer.Enabled = False
        obs.TransitionToProgram()

    End Sub

    Private Sub TbSzeneCurrent_TextChanged(sender As Object, e As EventArgs) Handles TbSzeneCurrent.TextChanged
        TxAktuellerScreen()
    End Sub

    Private Sub TxStatus()
        Dim st As OutputStatus

        st = obs.GetStreamingStatus
        If st.IsRecording <> letzterStatusIsRecording Then
            letzterStatusIsRecording = st.IsRecording

            TxRS232("#StatusRec" + st.IsRecording.ToString)


        End If
        If st.IsStreaming <> letzterStatusIsStreaming Then
            letzterStatusIsStreaming = st.IsStreaming
            TxRS232("#StatusStream" + st.IsStreaming.ToString)
        End If




    End Sub

    Private Function TxRS232(Text As String)
        Timer1.Stop()
        Dim retstr As String = ""
        Try
            RS1.WriteLine(Text)
            retstr = RS1.ReadLine
            TbRxRS232.Text = retstr + vbCrLf + TbRxRS232.Text
            GbCom.BackColor = Color.Lime

        Catch ex As Exception
            GbCom.BackColor = Color.Red
        End Try
        Threading.Thread.Sleep(100)
        Timer1.Start()
        Return retstr
    End Function

    Private Sub TxAktuellerScreen()
        Dim Lautsprecher As Boolean = False
        Dim video As Boolean = False
        Dim aktSzene As Integer
        If RS1.IsOpen = False Then Return
        For u = 0 To CbSzeneHidden.Items.Count - 1
            If CbSzeneHidden.Items(u) = TbSzeneCurrent.Text Then
                For o = 0 To ObsSzenenListe(u).Items.Count - 1
                    If ObsSzenenListe(u).Items(o).InternalType = "wasapi_input_capture" Then Lautsprecher = True
                    If ObsSzenenListe(u).Items(o).InternalType = "dshow_input" Then video = True
                    If video = True Then Lautsprecher = True
                    aktSzene = u

                Next

            End If
        Next

        TxRS232("#Ls" + Lautsprecher.ToString)
        TxRS232("#Vi" + video.ToString)

        TxRS232("#SA" + aktSzene.ToString("000"))
        TxRS232("#AL" + TbSzeneCurrent.Text)
    End Sub

    Private Sub TxPreviewScreen()
        If RS1.IsOpen = False Then Return

        TxRS232("#SP" + CbSzeneHidden.FindString(CbSzene.Text).ToString("000"))

        ScreenPrevTimer.Enabled = False
        ScreenPrevTimer.Enabled = True

    End Sub



    Private Sub LbNächsteSzene_TextChanged(sender As Object, e As EventArgs) Handles LbNächsteSzene.TextChanged
        TxPreviewText()
    End Sub

    Private Sub ScreenPrevTimer_Tick(sender As Object, e As EventArgs) Handles ScreenPrevTimer.Tick
        ScreenPrevTimer.Enabled = False
        TxAktuellerScreen()
        TxPreviewText()
    End Sub
    Private Sub TxPreviewText()
        If RS1.IsOpen = False Then Return
        TxRS232("#PL" + LbNächsteSzene.Text)

    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        OFD1.ShowDialog()
        Dim richtext As New RichTextBox
        richtext.LoadFile(OFD1.FileName, RichTextBoxStreamType.PlainText)
        Dim j As Object = New JavaScriptSerializer().Deserialize(Of Object)(richtext.Text)
        Dim obj As Object = j("sources")
        DsSceneItem = New DataSet
        TabSceneItem = New DataTable
        TabSceneItem.TableName = "SzenenItem"
        DsSceneItem.Tables.Add(TabSceneItem)


        Dim cb As New ComboBox



        Dim mylist As Object

        For i = 0 To obj.length - 1
            Dim row = TabSceneItem.NewRow
            TabSceneItem.Rows.Add(row)


            mylist = obj(i).Keys
            For Each item In mylist
                Dim key As String = item.ToString

                If cb.Items.Contains(key) = False Then
                    cb.Items.Add(key)
                    TabSceneItem.Columns.Add(key)
                End If
                Dim wert As Object = obj(i)(key)
                Try
                    If wert.count > 0 Then
                        json2Datagrid(wert, cb, row)

                    End If
                Catch ex As Exception

                End Try

                row(key) = wert
ende:
            Next

        Next

        dgv1.DataSource = TabSceneItem
    End Sub
    Private Sub json2Datagrid(Obj As Dictionary(Of String, Object), cb As ComboBox, row As DataRow)


        For i = 0 To Obj.Count - 1



            Dim key As String = Obj.Keys(i).ToString 

            If cb.Items.Contains(key) = False Then
                cb.Items.Add(key)
                TabSceneItem.Columns.Add(key)
            End If
            Dim wert As Object = Obj.Values(i)
            Try
                If wert.count > 0 Then
                    json2Datagrid(wert, cb, row)

                End If
            Catch ex As Exception

            End Try

            row(key) = wert
ende:
        Next


    End Sub
End Class
