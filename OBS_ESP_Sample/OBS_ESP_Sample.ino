/*
Simple WebSocket client for ArduinoHttpClient library
Connects to the WebSocket server, and sends a hello
message every 5 seconds
created 28 Jun 2016
by Sandeep Mistry
this example is in the public domain
*/
#include <ArduinoHttpClient.h>
#include <WiFi.h>
#include "geheim.h"


///////please enter your sensitive data in the Secret tab/arduino_secrets.h
/////// Wifi Settings ///////
const char* ssid = SECRET_SSID;
const char* pass = SECRET_PASS ;

char serverAddress[] = "X1THK";  // server address
int port = 4444;

WiFiClient wifi;
WebSocketClient client = WebSocketClient(wifi, serverAddress, port);
int status = WL_IDLE_STATUS;
int count = 0;

void setup() {
	Serial.begin(9600);
	WiFi.begin(ssid, pass);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}


	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi shield's IP address :
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);
}

void loop() {
	Serial.println("starting WebSocket client");
	client.begin();

	while (client.connected()) {
		Serial.print("GetVersion");
		Serial.println(count);

		// send a hello #
		client.beginMessage(TYPE_TEXT);
		client.print("{\"request-type\": \"GetCurrentScene\",\"message-id\": \"1\"}");

		client.endMessage();

		// increment count for next message
		count++;

		// check if a message is available to be received
		int messageSize = client.parseMessage();

		if (messageSize > 0) {
			Serial.println("Received a message:");
			Serial.println(client.readString());
		}

		// wait 5 seconds
		delay(5000);
	}

	Serial.println("disconnected");
}
