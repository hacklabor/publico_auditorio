/***************************************************
This is a example sketch demonstrating the graphics
capabilities of the SSD1331 library  for the 0.96"
16-bit Color OLED with SSD1331 driver chip

Pick one up today in the adafruit shop!
------> http://www.adafruit.com/products/684

These displays use SPI to communicate, 4 or 5 pins are required to
interface
Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!

Written by Limor Fried/Ladyada for Adafruit Industries.
BSD license, all text above must be included in any redistribution
****************************************************/
//Pikto
static const uint8_t  PROGMEM YouTube[] = { 
	0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 0xff, 0xe0, 0x1f, 0xff, 0xff, 0xf8, 0x39, 0xff, 0xff, 0xfc,
	0x38, 0x7d, 0xf7, 0xfc, 0x78, 0x1e, 0xef, 0xfe, 0x78, 0x07, 0x5f, 0xfe, 0x78, 0x03, 0xbf, 0xfe,
	0x78, 0x03, 0xb3, 0x6e, 0x78, 0x07, 0xad, 0x6e, 0x78, 0x1f, 0xad, 0x6e, 0x38, 0x7f, 0xb3, 0x9c,
	0x39, 0xff, 0xff, 0xfc, 0x1f, 0xff, 0xff, 0xf8, 0x07, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t  PROGMEM REC[] = {
0x0f, 0xff, 0xff, 0xf0, 0x38, 0x00, 0x00, 0x1c, 0x60, 0x00, 0x00, 0x06, 0x40, 0x00, 0x00, 0x02,
0xc0, 0x07, 0xbd, 0xe3, 0x83, 0x84, 0xa1, 0x01, 0x87, 0xc4, 0xa1, 0x01, 0x8f, 0xe7, 0xb9, 0x01,
0x8f, 0xe6, 0x21, 0x01, 0x87, 0xc5, 0x21, 0x01, 0x83, 0x84, 0xbd, 0xe1, 0xc0, 0x00, 0x00, 0x03,
0x40, 0x00, 0x00, 0x02, 0x60, 0x00, 0x00, 0x06, 0x38, 0x00, 0x00, 0x1c, 0x0f, 0xff, 0xff, 0xf0 };

static const uint8_t  PROGMEM LsEin[] = {
		0xff, 0xff, 0xff, 0xff, 0xff, 0x33, 0xfe, 0xb9, 0xfd, 0xad, 0x83, 0xa5, 0xbf, 0xb5, 0xbf, 0xb5,
	0xbf, 0xb5, 0x83, 0xa5, 0xfd, 0xad, 0xfe, 0xb9, 0xff, 0x33, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

static const uint8_t  PROGMEM LsAus[] = {
	0x3f, 0xfc, 0x1f, 0xf8, 0x8f, 0x31, 0xc6, 0xa1, 0xe1, 0x85, 0x81, 0x85, 0xb8, 0x15, 0xbc, 0x35,
	0xbc, 0x35, 0x80, 0x05, 0xf1, 0x8d, 0xe2, 0x81, 0xc7, 0x23, 0x8f, 0xf1, 0x1f, 0xf8, 0x3f, 0xfc };

static const uint8_t  PROGMEM CamEin[] = {
0xff, 0xff, 0xff, 0xff, 0xce, 0x7f, 0xb5, 0xbf, 0xb5, 0xbf, 0xce, 0x7f, 0xff, 0xff, 0xe0, 0x1f,
0xe0, 0x11, 0xe0, 0x11, 0xe0, 0x11, 0xe0, 0x11, 0xe0, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

static const uint8_t  PROGMEM CamAus[] = {
	0xc0, 0x03, 0xe0, 0x07, 0x71, 0x8e, 0x7a, 0x5c, 0x5e, 0x78, 0x3f, 0xf0, 0x07, 0xe0, 0x1f, 0xe0,
	0x1f, 0xee, 0x1f, 0xee, 0x1f, 0xfe, 0x1f, 0xfe, 0x3f, 0xfc, 0x70, 0x0e, 0xe0, 0x07, 0xc0, 0x03 };

static const uint8_t  PROGMEM Pause[] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7d,
0xbc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7d, 0xbc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7d,
0xbd, 0xfc, 0x08, 0x20, 0x43, 0xc3, 0xfc, 0x7d, 0xbd, 0x02, 0x1c, 0x20, 0x44, 0x22, 0x00, 0x7d,
0xbd, 0x02, 0x14, 0x20, 0x48, 0x32, 0x00, 0x7d, 0xbd, 0x02, 0x36, 0x20, 0x48, 0x02, 0x00, 0x7d,
0xbd, 0x06, 0x22, 0x20, 0x46, 0x02, 0x00, 0x7d, 0xbd, 0xfc, 0x22, 0x20, 0x41, 0xe3, 0xf8, 0x7d,
0xbd, 0x00, 0x63, 0x20, 0x40, 0x32, 0x00, 0x7d, 0xbd, 0x00, 0x7f, 0x20, 0x40, 0x12, 0x00, 0x7d,
0xbd, 0x00, 0x41, 0x20, 0xc8, 0x12, 0x00, 0x7d, 0xbd, 0x00, 0xc1, 0x90, 0x8c, 0x32, 0x00, 0x7d,
0xbd, 0x00, 0x80, 0x8f, 0x03, 0xc3, 0xfc, 0x7d, 0xbc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7d,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };



// 'PräsiGroß'
static const uint8_t  PROGMEM PraesiGross[] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x07, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x01, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x01, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

// 'PräsiGroß_SprecherKlein'
static const uint8_t  PROGMEM PraesiGross_SprecherKlein[] = {

0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x07, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x01, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xb0, 0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xed,
0xb7, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x01, 0xed, 0xb7, 0xe3, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xed,
0xb7, 0xdd, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xed, 0xb7, 0xaa, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0d,
0xb7, 0xbe, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0xdd, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb7, 0xe3, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0xeb, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb7, 0x9c, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x7f, 0x37, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb5, 0xff, 0xd7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb0, 0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

// 'PräsiKlein_SprecherGroß'
static const uint8_t  PROGMEM PraesiKlein_SprecherGross[] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0x83, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0x7d, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfe, 0xfe, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfd, 0xff, 0x7f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfd, 0xff, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0x39, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0x39, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfd, 0x7d, 0x7f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfd, 0x83, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfe, 0xfe, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0x3d, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xc3, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0x01, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfc, 0xfe, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xf3, 0xff, 0x8f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0x8f, 0xff, 0xf3, 0xff, 0xed, 0xbf, 0xf7, 0xfe, 0x7f, 0xff, 0xfc, 0x7f, 0xed,
0xbf, 0xf7, 0xf9, 0xff, 0xff, 0xff, 0x9f, 0xed, 0xb0, 0x00, 0x07, 0xff, 0xff, 0xff, 0xe7, 0xed,
0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xf8, 0xed, 0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0x2d,
0xb6, 0x00, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xcd, 0xb7, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0d,
0xb7, 0x00, 0x37, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb6, 0x00, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb7, 0x00, 0x37, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xb7, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb0, 0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

// 'SprecherGroß'
static const uint8_t  PROGMEM SprecherGross[] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0xbf, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0x83, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0x7d, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfe, 0xfe, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfd, 0xff, 0x7f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfd, 0xff, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0x39, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0x39, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfb, 0xff, 0xbf, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfd, 0x7d, 0x7f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfd, 0x83, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xfe, 0xfe, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0x3d, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xc3, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xff, 0x81, 0xff, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xfe, 0x7e, 0x7f, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0xf9, 0xff, 0x8f, 0xff, 0xed,
0xbf, 0xf7, 0xff, 0xe7, 0xff, 0xf3, 0xff, 0xed, 0xbf, 0xf7, 0xff, 0x9f, 0xff, 0xfc, 0x7f, 0xed,
0xbf, 0xf7, 0xfe, 0x7f, 0xff, 0xff, 0x9f, 0xed, 0xbf, 0xf7, 0xf9, 0xff, 0xff, 0xff, 0xe7, 0xed,
0xbf, 0xf7, 0xe7, 0xff, 0xff, 0xff, 0xf8, 0xed, 0xbf, 0xf7, 0x9f, 0xff, 0xff, 0xff, 0xff, 0x2d,
0xbf, 0xf6, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xcd, 0xbf, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

// 'Start'
static const uint8_t  PROGMEM Start[] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd,
0xbe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd, 0xbe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd,
0xbe, 0x3c, 0x7f, 0xc2, 0x0f, 0xe3, 0xfe, 0xfd, 0xbe, 0x42, 0x04, 0x07, 0x08, 0x10, 0x20, 0xfd,
0xbe, 0x83, 0x04, 0x05, 0x08, 0x18, 0x20, 0xfd, 0xbe, 0x80, 0x04, 0x0d, 0x88, 0x18, 0x20, 0xfd,
0xbe, 0x60, 0x04, 0x08, 0x88, 0x10, 0x20, 0xfd, 0xbe, 0x1e, 0x04, 0x08, 0x8f, 0xe0, 0x20, 0xfd,
0xbe, 0x03, 0x04, 0x18, 0xc8, 0x40, 0x20, 0xfd, 0xbe, 0x01, 0x04, 0x1f, 0xc8, 0x20, 0x20, 0xfd,
0xbe, 0x81, 0x04, 0x10, 0x48, 0x30, 0x20, 0xfd, 0xbe, 0xc3, 0x04, 0x30, 0x68, 0x18, 0x20, 0xfd,
0xbe, 0x3c, 0x04, 0x20, 0x28, 0x08, 0x20, 0xfd, 0xbe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd,
0xbe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

// 'Outro'
static const uint8_t  PROGMEM Outro[] = {
	// 'OUTRO'
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0x0f, 0xbf, 0x60, 0x30, 0x1f, 0xc3, 0xfd,
	0xbe, 0xf7, 0xbf, 0x7d, 0xf7, 0xef, 0xbd, 0xfd, 0xbd, 0xfb, 0xbf, 0x7d, 0xf7, 0xef, 0x7e, 0xfd,
	0xbb, 0xfd, 0xbf, 0x7d, 0xf7, 0xee, 0xff, 0x7d, 0xbb, 0xfd, 0xbf, 0x7d, 0xf7, 0xee, 0xff, 0x7d,
	0xbb, 0xfd, 0xbf, 0x7d, 0xf0, 0x1e, 0xff, 0x7d, 0xbb, 0xfd, 0xbf, 0x7d, 0xf7, 0xbe, 0xff, 0x7d,
	0xbb, 0xfd, 0xbf, 0x7d, 0xf7, 0xde, 0xff, 0x7d, 0xbd, 0xfb, 0xbf, 0x7d, 0xf7, 0xef, 0x7e, 0xfd,
	0xbe, 0xf3, 0xde, 0xfd, 0xf7, 0xef, 0xbd, 0xfd, 0xbf, 0x0f, 0xe1, 0xfd, 0xf7, 0xef, 0xc3, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd,
	0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };


String inputString = "";         // a String to hold incoming data
boolean stringComplete = false;  // whether the string is complete

// You can use any (4 or) 5 pins 
#include <gfxfont.h>
#define sclk 13
#define mosi 11
#define cs   10
#define rst  9
#define dc   8


// Color definitions
#define	BLACK           0x0000
#define	BLUE            0x001F
#define	RED             0xF800
#define	GREEN           0x07E0
#define CYAN            0x07FF
#define MAGENTA         0xF81F
#define YELLOW          0xFFE0  
#define WHITE           0xFFFF
#define LigthGray		0xBDD6

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1331.h>
#include <SPI.h>

// Option 1: use any pins but a little slower
//Adafruit_SSD1331 display = Adafruit_SSD1331(cs, dc, mosi, sclk, rst);  

// Option 2: must use the hardware SPI pins 
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be 
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_SSD1331 display = Adafruit_SSD1331(cs, dc, rst);



void setup(void) {
	Serial.begin(57600);
	Serial.print("hello!");
	display.begin();

	Serial.println("init OBS REMOTE V1.0");
	
	display.fillScreen(BLACK);
	

		display.fillScreen(WHITE);
	display.drawBitmap(64,0, YouTube, 32, 16, LigthGray);
	display.drawBitmap(64, 16, REC, 32, 16, LigthGray);
	display.drawBitmap(64, 32, LsEin, 16, 16, LigthGray);
	display.drawBitmap(64+16, 32,CamEin, 16, 16, LigthGray);
	text_zeichnen(0, 24, BLACK, WHITE, "Server?!");

	
	
}

void loop() {
	DATARX();
	if (stringComplete) {
		Auswertung();
		
	}
}
void DATARX (){
	while (Serial.available()) {
		// get the new byte:
		char inChar = (char)Serial.read();
		Serial.write(inChar);
		// add it to the inputString:
		inputString += inChar;
		// if the incoming character is a newline, set a flag so the main loop can
		// do something about it:
		if (inChar == '\n') {
			stringComplete = true;
		}
	}

}

void Auswertung() {

	if (inputString.substring(0, 1) != "#") { return; }
	if (inputString.substring(0, 3) == "#SA") {
		PiktoSzene(inputString.substring(3, 6), 1);
	}
	if (inputString.substring(0, 3) == "#SP") {
		PiktoSzene(inputString.substring(3, 6), 0);
		//PiktoSzene1(inputString.substring(3), 0);
	}

	if (inputString.substring(0, 3) == "#AL") {
		text_zeichnen(0, 48, BLUE, WHITE, inputString.substring(3));
	}
	if (inputString.substring(0, 3) == "#PL") {
		text_zeichnen(0, 48 + 8, BLACK, WHITE, inputString.substring(3));
	}
	if (inputString.substring(0, 14) == "#StatusRecTrue") {
		display.drawBitmap(64, 16, REC, 32, 16, RED);
	}
	if (inputString.substring(0, 15) == "#StatusRecFalse") {
		display.drawBitmap(64, 16, REC, 32, 16, LigthGray);
	}
	
	
	if (inputString.substring(0, 17) == "#StatusStreamTrue") {
		display.drawBitmap(64, 0, YouTube, 32, 16, RED);
	}
	if (inputString.substring(0, 18) == "#StatusStreamFalse") {
		display.drawBitmap(64, 0, YouTube, 32, 16, LigthGray);
	}

	if (inputString.substring(0, 7) == "#LsTrue") {
		display.drawBitmap(64, 32, LsEin, 16, 16, RED);
	}
	if (inputString.substring(0, 8) == "#LsFalse") {
		display.drawBitmap(64, 32, LsEin, 16, 16, LigthGray);

	}
	if (inputString.substring(0, 7) == "#ViTrue") {
		display.drawBitmap(64+16, 32, CamEin, 16, 16, RED);
	}
	if (inputString.substring(0, 8) == "#ViFalse") {
		display.drawBitmap(64+16, 32, CamEin, 16, 16, LigthGray);

	}

	

	inputString = "";
	stringComplete = false;
}

void text_zeichnen(uint16_t x, uint16_t y, uint16_t FontColor, uint16_t BackColor, String text) {


	display.setCursor(x, y);
	display.fillRect(x, y, 96, 8, BackColor);
	display.setTextColor(FontColor, BackColor);
	display.setTextSize(1);

		display.print(text);

}



void PiktoSzene (String Szene, uint8_t Aktuell){
	uint16_t Farbe;
	
	if (Aktuell == 0){Farbe = BLACK; }
	if (Aktuell == 1){Farbe = BLUE; }

	display.fillRect(0, 0, 64, 48, WHITE);
	
	if (Szene == "000") {
		display.drawBitmap(0, 0, Start, 64, 48, Farbe);
		return;
	}
	if (Szene == "001") {
		display.drawBitmap(0, 0, Pause, 64, 48, Farbe);
		return;
	}
	if (Szene == "002") {
		display.drawBitmap(0, 0, Outro, 64, 48, Farbe);
		return;
	}
	if (Szene == "006") {
		display.drawBitmap(0, 0, SprecherGross, 64, 48, Farbe);
		return;
	}
	if (Szene == "005") {
		display.drawBitmap(0, 0, PraesiKlein_SprecherGross, 64, 48, Farbe);
		return;
	}
	if (Szene == "004") {
		display.drawBitmap(0, 0, PraesiGross_SprecherKlein, 64, 48, Farbe);
		return;
	}
	if (Szene == "003") {
		display.drawBitmap(0, 0, PraesiGross, 64, 48, Farbe);
		return;
	}
	
	
}




